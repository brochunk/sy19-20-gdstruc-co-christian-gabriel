#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

int getSum(int number);

int fibo(int target, int first, int second)
{
	if (target <= 0)
	{
		return 0;
	}
	else if (target > 0)
	{
		cout << first << " ";
		fibo(target -= 1, second, first + second);
	}
}

bool primeCheck(int number, int divisor)
{

	if (number <= 2)
		return 0;
	if (number == 1)
		return true;

	else
	{
		if (number % divisor == 0)
			return false;
		else
			return primeCheck(number, divisor - 1);
	}
}


int main()
{
	int inputNumber;

	cout << "Input Number: ";
	cin >> inputNumber;

	cout << getSum(inputNumber);

	cout << "\nInput Nth Term: ";
	cin >> inputNumber;

	fibo(inputNumber, 0, 1);

	cout << "\nCheck Number if Prime: ";
	cin >> inputNumber;

	if (primeCheck(inputNumber, inputNumber))
	{
		cout << "\nNumber is composite number.";
	}

	else
	{
		cout << "\nNumber is a prime number.\n";
		system("pause");
		return 0;
	}
}

int getSum(int number)
{

	if (number == 0)
	{
		return 0;
	}

	else
	{
		return ((number % 10) + getSum(number / 10));
	}

}
