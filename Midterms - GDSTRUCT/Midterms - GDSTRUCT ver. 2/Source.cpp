

#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void printArray(int& arrayRef, int arraySizeRef)
{
	for (int i = 0; i < arraySizeRef; i++)
	{
		cout << arrayRef << " ";
		arrayRef++;
	}
}

void main()
{
	cout << "Enter size for dynamic array: ";
	int size;
	cin >> size;

	int userInput = 0;
	int result = 0;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}


	system("cls");

	while (true)
	{
		cout << "Unordered contents: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered contents: ";
		for (int i = 0; i < ordered.getSize(); i++)
		{
			cout << ordered[i] << "  ";
		}

		cout << "\n\nWhat do you want to do? "
			<< " \n [1] - Remove Element from Index"
			<< " \n [2] - Search for Element"
			<< " \n [3] - Expand and generate random value/s \n";

		cout << "\n Your Choice: ";
		cin >> userInput;
		cout << " \n \n";

		switch (userInput)
		{
		case 1:
			cout << "Enter index to remove: ";
			cin >> userInput;
			cout << endl << "Element removed: " << userInput << endl;

			unordered.remove(userInput);
			ordered.remove(userInput);

			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
			{
				cout << ordered[i] << " ";
			}

			cout << endl;
			system("pause");
			system("cls");
			break;
		case 2:

			cout << "Input element to search: ";
			cin >> userInput;

			result = unordered.linearSearch(userInput);
			/*cout << "Linear Search took " <<  << "comparisons." << endl;
			cout << "Binary Search took " <<  << "comparisons." << endl;*/
			if (result < 0)
				cout << userInput << " not found \n";
			else
			{
				cout << "\nElement " << userInput << " found.";
				result = unordered.linearSearch(userInput);
				cout << " Index " << result << " for Unordered Array,";
				result = ordered.binarySearch(userInput);
				cout << " index " << result << " for Ordered Array. " << endl;
			}

			system("pause");
			system("cls");
			break;
		case 3:
			cout << "\n\nInput size of expansion: ";
			cin >> userInput;
			cout << endl;

			for (int i = 0; i < userInput; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";

			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
			{
				cout << ordered[i] << " ";
			}

			cout << endl;
			system("pause");
			system("cls");
			break;

		default:
			cout << "Please choose only from the given." << endl;
			system("pause");
			system("cls");
			break;
		}

		userInput = NULL;
	}
}

