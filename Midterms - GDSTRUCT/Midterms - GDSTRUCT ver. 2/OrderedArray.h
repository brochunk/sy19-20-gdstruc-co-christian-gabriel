#pragma once
#include <assert.h>

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();


		mArray[mNumElements] = value;
		mNumElements++;



		for (int i = 0; i < mNumElements; i++)
		{
			for (int j = i + 1; j < mNumElements; j++)
			{
				if (mArray[i] > mArray[j])
				{
					int temp = mArray[i];
					mArray[i] = mArray[j];
					mArray[j] = temp;
				}
			}
		}
		// modified code from geeksforgeeks.org (bubble sort)
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	virtual int binarySearch(T val)
	{
		int comparisons = 0;
		int left = 0;
		int right = mMaxSize - 1;

			while (left <= right)
			{
				int mid = left + (right - left) / 2;

				if (mArray[mid] == val)
					return mid;


				if (mArray[mid] < val)
					left = mid + 1;

				else
					right = mid - 1;

				comparisons++;
			}
			// modified version of the code from geeksforgeeks.org (binary search)
			return -1;
	}

private:
	T* mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};