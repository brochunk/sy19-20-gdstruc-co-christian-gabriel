#pragma once

#include <assert.h>
#include <iostream>
#include <conio.h>

using namespace std; 

template<class T>
class UnorderedArray
{
public:
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~UnorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T& val)
	{
		assert(mArray != NULL);
		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = val;
		mNumElements++;
	}

	void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	void popFront()
	{

		for (int i = 0; i < mNumElements; i++)
		{
			if (mArray[0])
			{
				for (int j = i; j < (mNumElements - 1); j++)
				{
					mArray[j] = mArray[j + 1];
				}
				break;
			}
		}

		if (mNumElements > 0)
		{
			mNumElements--;
		}

	}
	void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize)
		{
			return;
		}

		for (int i = index; i < mMaxSize - 1; i++)
		{
			mArray[i] = mArray[i + 1];
		}

		if (mNumElements >= mMaxSize)
		{
			mNumElements = mMaxSize - 1;
		}
	}

	virtual int linearSearch(int index)
	{
		for (int i = 0; i < mMaxSize; i++)
			if (mArray[i] == index)
				return index;
		return -1;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);
		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

private:
	T* mArray;
	int mMaxSize;
	int mGrowSize;
	int mNumElements;
};