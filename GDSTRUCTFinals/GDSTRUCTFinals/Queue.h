#pragma once


#include <iostream>
#include <conio.h>
#include "UnorderedArray.h"

using namespace std;

template <class T>
class Queue
{
public:
	Queue(int size)
	{
		queue = new UnorderedArray<T>(size);
	}

	virtual void push(T value)
	{
		 queue->push(value);
	}

	void pop()
	{
		queue->popFront();
	}

	void print()
	{
		cout << "Queue: ";
		;
		for (int i = 0; i < queue->getSize(); i++)
		{
			cout << (*queue)[i] << " ";

		}
	}

	void empty()
	{
		for (int i = 0; i < queue->getSize(); i++)
		{
			queue->pop();

		}
	}
	virtual int top()
	{
		return (*queue)[0];
	}

private:
	UnorderedArray<T>* queue;
};