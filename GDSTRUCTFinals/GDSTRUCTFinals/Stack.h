#pragma once

#include <iostream>
#include <conio.h>
#include "UnorderedArray.h"

using namespace std;

template <class T>
class Stack
{
public:
	Stack(int size)
	{
		stack = new UnorderedArray<T> (size);
	}

	virtual void push(T value)
	{
		stack->push(value);
	}

	void pop()
	{
		stack->pop();
	}

	void print()
	{
		cout << "Stack: ";
		for (int i = stack->getSize() - 1; i  > -1 ; i--)
		{
			cout << (*stack)[i] << " ";
			
		}
	}

	void empty()
	{
		for (int i = stack->getSize(); i > -1; i--)
		{
			stack->pop();

		}
	}

	virtual int top()
	{
		int top = stack->getSize()-1;
		return (*stack)[top];
	}

private:
	UnorderedArray<T>* stack;
}; 