#include <iostream>
#include <conio.h>

#include "UnorderedArray.h"
#include "Stack.h"
#include "Queue.h"

using namespace std;

int main()
{
	int input;
	int size = 0;
	cout << "Enter initial size of element sets: ";
	cin >> size;

	Queue <int> newQueue(size);
	Stack <int> newStack(size);

	while (1)
	{
		cout << "1 - Push Element \n" << "2 - Pop Element \n" << "3 - Print Everything the Empty Set \n";
		cin >> input;
		cout << "\n \n";

		switch (input)
		{
		case 1:
			cout << "Enter Number: ";
			cin >> input;

			newQueue.push(input);
			newStack.push(input);

			cout << "\nThe top elements of set: \n";
			cout << "Queue: " << newQueue.top() << "\n";
			cout << "Stack: " << newStack.top() << "\n";
			system("pause");
			system("cls");

			break;
		case 2:

			newQueue.pop();
			newStack.pop();
			
			cout << "You have popped the front elements! \n";
			system("pause");
			system("cls");
			break;

		case 3:
			newQueue.print();
			cout << "\n";
			newStack.print();
			cout << "\n \n";

			newQueue.empty();
			newStack.empty();

			system("pause");
			system("cls");
			break;

		default:
			cout << "You can only choose from 1 - 3 \n";
			system("pause");
			system("cls");
		}

	}

	system("pause");

}