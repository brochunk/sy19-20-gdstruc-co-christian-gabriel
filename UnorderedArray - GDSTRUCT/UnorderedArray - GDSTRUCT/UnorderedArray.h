#pragma once

#include <assert.h>


template<class T> //template - allows class to accept data class
class UnorderedArray
{
public:
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)//faster if initiallized here rather than inside - Sir Dale
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);//sets memory for mArray 
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~UnorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;//if just delete then it will just delete 1st element
			mArray = NULL;
		}
	}//deconstructor

	virtual void push(T val)//adds element sa huli
	{
		assert(mArray != NULL);//if the array is present, we push a value
		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = val;//add value to the last position
		mNumElements++;
	}

	void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize) //for safe guarding
		{
			return;
		}

		for (int i = index; i < mMaxSize - 1; i++) //shifting elements from the right to the left
		{
			mArray[i] = mArray[i + 1];
		}

		if (mNumElements >= mMaxSize)
		{
			mNumElements = mMaxSize - 1;
		}
	}

	virtual int linearSearch(int index)
	{
			for (int i = 0; i < mMaxSize; i++)
				if (mArray[i] == index)
					return index;
			return -1;
	}
	
	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);
		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

private:
	T* mArray; // T is any data type we pass. Can be any letter. But usually naming convention T then Q if there is another
	int mMaxSize;
	int mGrowSize;
	int mNumElements;
};